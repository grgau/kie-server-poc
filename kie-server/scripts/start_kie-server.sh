#!/usr/bin/env bash

# Add users
./add-user.sh -a -u kieserver -p kieserver1! -g kie-server;
./add-user.sh -a -u workbench -p workbench! -g admin,kie-server;

# Start WildFly with some parameters.
./standalone.sh -b $JBOSS_BIND_ADDRESS -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses=true
exit $?